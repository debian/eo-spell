#!/usr/bin/perl -w
# -*- coding: iso-8859-3 -*-

while (<>){
  my ($root, $flags) = split('/',$_);
  # Process roots
  next if $root =~ /\\o/;
  $root =~ s/\^c/�/g;
  $root =~ s/\^C/�/g;
  $root =~ s/\^g/�/g;
  $root =~ s/\^G/�/g;
  $root =~ s/\^h/�/g;
  $root =~ s/\^H/�/g;
  $root =~ s/\^j/�/g;
  $root =~ s/\^J/�/g;
  $root =~ s/\^s/�/g;
  $root =~ s/\^S/�/g;
  $root =~ s/\^u/�/g;
  $root =~ s/\^U/�/g;
  # Some words in eo.dic are written in TeX encoding. Convert to latin3
  $root =~ s/\\"u/�/g;
  $root =~ s/\\c\{c\}/�/g;
  $root =~ s/\\'a/�/g;
  if ( $flags ){
    # Process flags
    $flags =~ s/\\/!/;      # Rename flag '\' to '! for aspell benefit'
    print join ('/', $root, $flags);
  } else {
    print $root;
  }
}
