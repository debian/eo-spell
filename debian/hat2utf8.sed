# -*- coding: utf-8 -*-
# Protect things like [^g] or [^j], which mean not{g,j}
s/\[^/\[\$\$/g
# Now real hat2utf8 stuff
s/\^c/ĉ/g
s/\^g/ĝ/g
s/\^h/ĥ/g
s/\^j/ĵ/g
s/\^s/ŝ/g
s/\^u/ŭ/g
s/\^C/Ĉ/g
s/\^G/Ĝ/g
s/\^H/Ĥ/g
s/\^J/Ĵ/g
s/\^S/Ŝ/g
s/\^U/Ŭ/g
# Undo above protection
s/\[\$\$/\[^/g
# Some words in eo.dic are written in TeX encoding. Convert to utf8
s/\\"u/ü/g
s/\\c{c}/ç/g
s/\\'a/á/g
s/{\\o}/ĝ/g
