# -*- coding: iso-8859-3 -*-
# Protect things like [^g] or [^j], which mean not{g,j}
s/\[^/\[\$\$/g
# Now real hat2latin3 stuff
s/\^c/�/g
s/\^g/�/g
s/\^h/�/g
s/\^j/�/g
s/\^s/�/g
s/\^u/�/g
s/\^C/�/g
s/\^G/�/g
s/\^H/�/g
s/\^J/�/g
s/\^S/�/g
s/\^U/�/g
# Undo above protection
s/\[\$\$/\[^/g
# Some words in eo.dic are written in TeX encoding. Convert to latin3
s/\\"u/�/g
s/\\c{c}/�/g
s/\\'a/�/g
